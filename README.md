# Description
This repo contains the code for gerenating the CHANGELOG file from Gitlab merge requests.

It finds the previous merge pipeline and lists the merge requests since then. Then parses the merge requests' title according the [Angular commit style](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#commit-message-format) and generates a markdown file with sections corresponding to the found types and scopes. The file is committed to the repo.

# How to use
```
docker run -e GITLAB_TOKEN=$GITLAB_TOKEN -e PROJECT_ID=$PROJECT_ID -e VERSION=$VERSION psamim/changelog
```

## Variables
| Name                    | Required? | Desc.                                                                                 |
| ----------------------- | --------- | ------------------------------------------------------------------------------------- |
| GITLAB_TOKEN            | Yes       | Access token for reading merge requests and committs                                  |
| PROJECT_ID              | Yes       | Project ID for getting merge requests                                                 |
| VERSION                 | No        | Version to display in changelog. Derived from git ref (branch or tag) if not provided |
| CHANGELOG_PATH          | No        | Defaults to `CHANGELOG.md`                                                            |
| CHANGELOG_BRANCH        | No        | Branch to commit changelog, defaults to `master`                                      |
| PIPELINE_PROJECT_BRANCH | No        | Branch to check for last pipeline, defaults to `prod`                                 |
| PIPELINE_PROJECT_ID     | No        | Project to check for last pipeline, defaults to PROJECT_ID                            |

## Sample `.gitlab-ci.yaml`
```
changelog:
  image: docker:latest
  services:
    - docker:dind
  script:
    - VERSION=$(grep 'version' package.json | cut -d '"' -f4)
    - touch CHANGELOG.md
    - > 
      docker run 
      -e GITLAB_TOKEN=$GITLAB_TOKEN 
      -e PROJECT_ID=$CI_PROJECT_ID
      -e VERSION=$VERSION
      psamim/changelog
```

