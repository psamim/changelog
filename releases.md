## [{{version}}] - {{date}}
{%- for t in types %}
### {{t.type | title}} 
{%- for s in t.scopes %}
{%- if s.scope != "other" or t.type != "other" %}
{{'#### ' + s.scope | title}} 
{%- endif %} 
{%- for m in s.messages %}
- {{m.title | trim | capitalize}} ([{{m.id}}]({{m.url}}))
{%- endfor %} {# messages #}
{{-'\n'}} {# blank line #}
{%- endfor %} {# scopes #}
{{-'\n'}} {# blank line #}
{%- endfor %} {# types #}
