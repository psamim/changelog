import re
from jinja2 import Template
from dateutil import parser
from datetime import date
import itertools
from operator import itemgetter
import os
import gitlab
from semantic_release.history.parser_angular import parse_commit_message

gitlab_instance = None
pipeline_instance = None
project_instance = None


def get_gitlab_instance():
    global gitlab_instance
    if gitlab_instance is None:
        gitlab_instance = gitlab.Gitlab('https://gitlab.com',
                                        private_token=GITLAB_TOKEN)
    return gitlab_instance


def get_project():
    global project_instance
    gl = get_gitlab_instance()
    if project_instance is None:
        project_instance = gl.projects.get(PROJECT_ID)
    return project_instance


def get_last_pipeline():
    global pipeline_instance
    if pipeline_instance is None:
        project = get_project()
        pipelines = project.pipelines.list(ref="prod", status="success")

        if len(pipelines) == 0:
            return None

        last_pipeline = pipelines[0]
        last_pipeline_id = last_pipeline.id
        pipeline_instance = project.pipelines.get(last_pipeline_id)
    return pipeline_instance


def get_version():
    pl = get_last_pipeline()
    ref = pl.attributes.get("ref")
    return ref


def get_release_date():
    pipeline = get_last_pipeline()
    finished_date = parser.parse(pipeline.finished_at)
    return finished_date


def get_mergerequests_after(after_date):
    project = get_project()
    prs = project.mergerequests.list(state="merged",
                                     wip="no",
                                     all=True,
                                     updated_after=after_date)
    return prs


def mergerequest_to_message(mr):
    title = mr.title
    url = mr.web_url
    id = mr.iid
    match = re.match('Resolve "(.*)"', title)
    if (match):
        title = match.group(1)
    try:
        _, type, scope, descs = parse_commit_message(title)
        scope = scope or "miscellaneous"
        scope = scope.lower()
        scope = scope.strip()
        scope = re.sub(' +', '-', scope)
        return {
            "type": type.lower(),
            "scope": scope,
            "title": " ".join(descs),
            "url": url,
            "id": id,
        }
    except:
        return {
            "type": "other",
            "scope": "other",
            "title": title,
            "url": url,
            "id": id
        }


def group_by(messages, group_key):
    sorted_messages = sorted(messages, key=itemgetter(group_key))
    groups = []
    for key, group in itertools.groupby(sorted_messages,
                                        key=lambda x: x[group_key]):
        groups.append({group_key: key, "messages": list(group)})

    return groups


def get_changelog_file():
    project = get_project()
    file = None
    try:
        file = project.files.get(file_path=CHANGELOG_PATH,
                                 ref=CHANGELOG_BRANCH)
    except gitlab.exceptions.GitlabGetError:
        file = project.files.create({
            'file_path': CHANGELOG_PATH,
            'branch': CHANGELOG_BRANCH,
            'content': '',
            'commit_message': 'Update CHANGELOG'
        })

    return file


def get_next_changelog():
    global pwd
    after_date = get_release_date()
    mergerequests = get_mergerequests_after(after_date)

    if len(mergerequests) == 0:
        print("No merge requests since " + after_date.isoformat())
        return None

    messages = list(map(mergerequest_to_message, mergerequests))
    type_texts = {"feature": "Added", "fix": "Fixed", "perf": "Performance"}

    types = group_by(messages, "type")
    types = list(
        map(
            lambda g: {
                "type": type_texts.get(g["type"], g["type"]),
                "scopes": group_by(g["messages"], "scope")
            }, types))

    with open(pwd + '/releases.md', 'r') as template:
        template = Template(template.read())
        template.environment.lstrip_blocks = True
        template.environment.trim_blocks = True
        x = template.render(types=types,
                            version=VERSION,
                            date=date.today().isoformat())
        return x

    return None


def create_next_changelog():
    next = get_next_changelog()
    if next is not None:
        current = get_changelog_file()
        current.content = next + "\n" + current.decode().decode()
        current.save(branch=CHANGELOG_BRANCH,
                     commit_message='Update CHANGELOG')


GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
PROJECT_ID = os.environ['PROJECT_ID']
VERSION = os.environ.get('VERSION', get_version())
PIPELINE_PROJECT_ID = os.environ.get("PIPELINE_PROJECT_ID", PROJECT_ID)
PIPELINE_PROJECT_BRANCH = os.environ.get("PIPELINE_PROJECT_BRANCH", "prod")
CHANGELOG_PATH = os.environ.get("CHANGELOG_PATH", "CHANGELOG.md")
CHANGELOG_BRANCH = os.environ.get("CHANGELOG_BRANCH", "master")
pwd = os.path.dirname(os.path.realpath(__file__))
create_next_changelog()
