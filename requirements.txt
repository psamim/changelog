bleach==3.1.0
certifi==2019.3.9
chardet==3.0.4
click==6.7
colorama==0.4.1
docutils==0.14
epc==0.0.5
gitdb2==2.0.5
GitPython==2.1.11
idna==2.8
importmagic==0.1.7
invoke==0.11.1
Jinja2==2.10.1
MarkupSafe==1.1.1
ndebug==0.1.0
pkginfo==1.5.0.1
Pygments==2.3.1
python-dateutil==2.8.0
python-gitlab==1.8.0
python-semantic-release==4.1.1
readme-renderer==24.0
requests==2.21.0
requests-toolbelt==0.9.1
semver==2.8.1
sexpdata==0.0.3
six==1.12.0
smmap2==2.0.5
tqdm==4.31.1
twine==1.13.0
urllib3==1.24.1
webencodings==0.5.1
